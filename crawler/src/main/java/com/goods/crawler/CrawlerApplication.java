package com.goods.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CrawlerApplication {
	private static Logger logger = LoggerFactory.getLogger(CrawlerApplication.class);

	public static void main(String[] args) {
       logger.info("start project...");
		SpringApplication.run(CrawlerApplication.class, args);
	}

}

