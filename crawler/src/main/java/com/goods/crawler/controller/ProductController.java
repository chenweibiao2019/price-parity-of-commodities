package com.goods.crawler.controller;

import com.goods.crawler.api.DTO.CrawlerRequestDTO;
import com.goods.crawler.facade.ProductFacade;
import com.goods.crawler.facade.ProductJDFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;

/**
 * <pre>类名: ProductController</pre>
 * <pre>描述: 商品控制器</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/1/20 20:58</pre>
 * <pre>作者: chenwb</pre>
 */
@RestController
@RequestMapping("/crawler")
public class ProductController {

	private static Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductFacade productFacade;

	@Autowired
	private ProductJDFacade productJDFacade;

	/**
	 * @param
	 * @return 输出参数
	 * @Description: 测试
	 * @author chenwb
	 * @date 2019/1/21 10:13
	 */
	@RequestMapping("/hello")
	public String Hello() {
		return "hello world!";
	}

	/**
	 * @param crawlerRequestDTO
	 * @return void
	 * @Description: 爬取天猫主流程入口
	 * @author chenwb
	 * @date 2019/1/21 10:48
	 */
	@RequestMapping("/startCrawler")
	public void startCrawler(@RequestBody CrawlerRequestDTO crawlerRequestDTO) throws IOException {
		productFacade.startCrawler(crawlerRequestDTO);
	}

	/**
	 * @param crawlerRequestDTO
	 * @return void
	 * @Description: 爬取主流程入口
	 * @author chenwb
	 * @date 2019/1/21 10:48
	 */
	@RequestMapping("/startCrawlerJD")
	public void startCrawlerJD(@RequestBody CrawlerRequestDTO crawlerRequestDTO) throws IOException {
		productJDFacade.startCrawlerJD(crawlerRequestDTO);
	}

	/**
	 * @Description: 补充爬取
	 * @author chenwb
	 * @date 2019/2/20 15:36
	 * @return void
	 */
	@RequestMapping("/additionCrawler")
	public void additionCrawler(){
			productFacade.additionCrawler();
	}

}
