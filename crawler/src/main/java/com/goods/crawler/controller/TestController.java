package com.goods.crawler.controller;

import com.goods.crawler.facade.IpTransferFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

	@Autowired
	private IpTransferFacade ipTransferFacade;

	@RequestMapping("/crawlerProxyIp")
	public void test1(){
		ipTransferFacade.crawlerDynamicIp();
	}
}
