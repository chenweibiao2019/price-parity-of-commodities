package com.goods.crawler.api.DTO;

import java.io.Serializable;

/**
* <pre>类名: CrawlerRequestDTO</pre>
* <pre>描述: 爬虫请求DTO</pre>
* <pre>版权: 浙江理工大学信息学院</pre>
* <pre>日期: 2019/1/27 17:44</pre>
* <pre>作者: chenwb</pre>
*/
public class CrawlerRequestDTO implements Serializable {

	/**
	 * 爬取URl
	 */
	private String url;

	/**
	 * 获取url
	 *
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置url
	 *
	 * @param url url
	 */
	public void setUrl(String url) {
		this.url = url;
	}


}
