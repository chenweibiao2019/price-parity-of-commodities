package com.goods.crawler.facade;

import com.goods.crawler.api.DTO.CrawlerRequestDTO;
import com.goods.crawler.entity.Product;
import com.goods.crawler.service.CategoryService;
import com.goods.crawler.service.IpTransferService;
import com.goods.crawler.service.ProductJDService;
import com.goods.crawler.service.ProductService;
import com.goods.crawler.utils.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <pre>类名: ProductJDFacade</pre>
 * <pre>描述: 爬虫Facade类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/15 15:12</pre>
 * <pre>作者: chenwb</pre>
 */
@Service
public class ProductJDFacade {

	private static Logger logger = LoggerFactory.getLogger(ProductJDFacade.class);

	@Autowired
	private ProductJDService productJDService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private IpTransferService ipTransferService;


	/**
	 * @param crawlerRequestDTO
	 * @return void
	 * @Description: 爬取主流程入口
	 * @author chenwb
	 * @date 2019/1/21 15:25
	 */
	public void startCrawlerJD(CrawlerRequestDTO crawlerRequestDTO) throws IOException {
		// 获取浏览器驱动
		WebDriver driver = WebDriverFactory.getInstance();
		// 查询关键词
		List<String> keys = categoryService.findAllCategory();
/*		List<String> keys = new ArrayList<String>();
		keys.add("小米手机");
		keys.add("羽绒服男");*/
		while (true) {

			l:
			for (String key : keys) {
				//校验ip是否有效
/*				try {
					driver.get(crawlerRequestDTO.getUrl());
					new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("mallSearch")));
					driver.findElement(By.id("mallSearch"));
					logger.info("----------ip有效,可以继续爬取---------------");
				} catch (Exception e) {
					driver = ipTransferService.getNewInstance();
				}*/

				try {
					// 提交关键词至搜索结果页
					this.basicOperation(crawlerRequestDTO, driver, key);
					// 爬取搜索结果信息
					this.keyCrawler(driver, key);
					while (true) {
						// 等待回到原来搜索结果页
						new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.className("gl-item")));
						int curPage = productJDService.getCurPageNum(driver);
						int pageTotal = productJDService.getPageTotal(driver);
						// 非末页，则点击下一页
						if (pageTotal >= curPage) {
							productJDService.toNextPage(driver);
							new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.className("gl-item")));
							if (curPage == productJDService.getCurPageNum(driver)) {
								continue l;
							}
							new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.className("gl-item")));
							this.keyCrawler(driver, key);
						} else {
							/*driver.quit();*/
							continue l;
						}
					}
				} catch (Exception e) {
					//不做处理
					e.printStackTrace();
					/*driver.quit();*/
				}
			}
		}
	}

	/**
	 * @param driver
	 * @param key
	 * @return void
	 * @Description: 爬取某个关键词的搜索结果
	 * @author chenwb
	 * @date 2019/1/23 15:22
	 */
	public void keyCrawler(WebDriver driver, String key) {
		new WebDriverWait(driver, 20, 3000).until(ExpectedConditions.presenceOfElementLocated(By.className("gl-item")));
		// 获取搜索结果页当页数据
		List<WebElement> searchResults = ((FirefoxDriver) driver).findElementsByClassName("gl-item");
		// 获取当前url，便于当页数据爬完后返回
		String curUrl = driver.getCurrentUrl();
		List<String> urls = new ArrayList<String>();
		for (WebElement element : searchResults) {
			urls.add(element.findElement(By.className("p-img")).findElement(By.tagName("a")).getAttribute("href"));
		}
		// 对当页商品进行爬取
		for (String url : urls) {
			Product product = productJDService.crawlingOne(driver, url);
			Product productTemp = productJDService.findProductByProductId(product.getProductId());
			productJDService.saveProductInfo(product, productTemp, key);
		}
		// 回到搜索结果页
		driver.get(curUrl);
	}

	/**
	 * @param crawlerRequestDTO
	 * @return WebDriver
	 * @Description: 获取关键词查询，并返回driver
	 * @author chenwb
	 * @date 2019/1/23 14:54
	 */
	private WebDriver basicOperation(CrawlerRequestDTO crawlerRequestDTO, WebDriver driver, String key) {
		// 隐性等待
//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(crawlerRequestDTO.getUrl());
		// 判断是否登录
/*		if (driver.findElement(By.className("sn-register")) != null) {
			driver.findElement(By.className("sn-login")).click();
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("J_SubmitStatic")));
			try {
				Thread.sleep(20);
				driver.findElement(By.id("TPL_username_1")).sendKeys("我叫陈伟标44");
				driver.findElement(By.id("TPL_password_1")).sendKeys("xiaoshagua10.31");
				Thread.sleep(20);
				driver.findElement(By.id("J_SubmitStatic")).click();
				new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.className("s-combobox-input")));
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/

		// 输入关键词
		WebElement inputelement = driver.findElement(By.id("key"));
		inputelement.sendKeys(key);
		// 表单提交
		WebElement searchTop = driver.findElement(By.className("button"));
		searchTop.click();
		return driver;
	}
}
