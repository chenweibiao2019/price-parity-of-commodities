package com.goods.crawler.utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.springframework.beans.factory.annotation.Value;

public class WebDriverFactory {

	public String BROWSER_PATH="D:\\ruanjian\\firefox\\firefox.exe";

	public String BROWSER_DRIVER_PATH="D:\\webDriver\\geckodriver-v0.23.0-win32\\geckodriver.exe";

	private static volatile WebDriver driver;

	private WebDriverFactory(){
		System.setProperty("webdriver.firefox.bin",BROWSER_PATH);
		FirefoxOptions options = new FirefoxOptions();
		options.setBinary(BROWSER_PATH);    //导入firefox安装路径

		FirefoxProfile profile = new FirefoxProfile();

		// 调用profile对象的setPreference方法，设定浏览器启动时首页显示为sogou
		//profile.setPreference("permissions.default.image", 2);
		options.setProfile(profile);
		//options.setHeadless(true);

		System.setProperty("webdriver.gecko.driver",BROWSER_DRIVER_PATH);
		driver = new FirefoxDriver(options);
	}

	public static WebDriver getInstance() {
		/*if (driver == null) {
			synchronized (WebDriverFactory.class) {
				if (driver == null) {*/
					new WebDriverFactory();
/*				}
			}
		}*/
		return driver;
	}
}
