package com.goods.crawler.utils.common;


import com.goods.crawler.utils.Enum.ResultEnum;

/**
 * <pre>类名: ResponseUtils</pre>
 * <pre>描述: 构建返回信息工具类
 * <pre>版权: ***</pre>
 * <pre>日期: 2018/12/14 15:09</pre>
 * <pre>作者: chenwb</pre>
 */
public class Response {

	public static Result success(){
		return new Result(ResultEnum.SUCCESS.getCode(),ResultEnum.SUCCESS.getMessage());
	}

	public static Result fail(){
		return new Result(ResultEnum.FAIL.getCode(),ResultEnum.FAIL.getMessage());
	}

	public static Result success(Object object){
		return new Result(ResultEnum.SUCCESS.getCode(),ResultEnum.SUCCESS.getMessage(),object);
	}
}
