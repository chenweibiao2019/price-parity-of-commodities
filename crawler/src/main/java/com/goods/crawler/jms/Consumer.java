package com.goods.crawler.jms;

import com.goods.crawler.api.DTO.ModuleInfoDTO;
import com.goods.crawler.utils.SystemUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * <pre>类名: Consumer</pre>
 * <pre>描述: 消息接收类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/25 20:44</pre>
 * <pre>作者: chenwb</pre>
 */
@Component
public class Consumer {

	@Autowired
	SystemUtil systemUtil;

	/**
	 * @param text
	 * @return void
	 * @Description: 消息监听
	 * @author chenwb
	 * @date 2019/2/25 20:46
	 */
	@JmsListener(destination = "getModulesInfo.topic",containerFactory = "myJmsContainerFactory")
	@SendTo("modulesInfo.queue")
	public String receiveQueue(String text) {
		System.out.println("Consumer收到的报文为:" + text);
		ModuleInfoDTO moduleInfoDTO = systemUtil.getModuleInfo();
		String result = "";
		result += moduleInfoDTO.getModuleName() + "," + moduleInfoDTO.getModuleIp() + "," + moduleInfoDTO.getModuleLatency() + "," + moduleInfoDTO.getOccupiedRateOfBandwidth() + "," + moduleInfoDTO.getOccupiedRateOfMemory() + "," + moduleInfoDTO.getOccupiedRateOfCpu() + "," + moduleInfoDTO.getModuleCondition();

		return result;
	}
}
