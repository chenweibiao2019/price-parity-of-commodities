package com.goods.crawler.repository;

import com.goods.crawler.entity.Product;
import com.goods.crawler.entity.Proxyip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <pre>类名: ProductsRepository</pre>
 * <pre>描述: ProxyIp接口类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/17 18:01</pre>
 * <pre>作者: chenwb</pre>
 */

@Repository
public interface ProxyIpRepository extends JpaRepository<Proxyip, Integer> {

}
