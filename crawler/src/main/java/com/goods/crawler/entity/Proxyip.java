package com.goods.crawler.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Proxyip {
	private int id;
	private String ipAddress;
	private String ipPort;
	private String serverAddress;
	private String checkTime;
	private Date updateTime;

	/**
	 * 获取id
	 *
	 * @return id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	/**
	 * 设置id
	 *
	 * @param id id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 获取ipAddress
	 *
	 * @return ipAddress
	 */
	@Basic
	@Column(name = "ip_address")
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * 设置ipAddress
	 *
	 * @param ipAddress ipAddress
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * 获取ipPort
	 *
	 * @return ipPort
	 */
	@Basic
	@Column(name = "ip_port")
	public String getIpPort() {
		return ipPort;
	}

	/**
	 * 设置ipPort
	 *
	 * @param ipPort ipPort
	 */
	public void setIpPort(String ipPort) {
		this.ipPort = ipPort;
	}

	/**
	 * 获取serverAddress
	 *
	 * @return serverAddress
	 */
	@Basic
	@Column(name = "server_address")
	public String getServerAddress() {
		return serverAddress;
	}

	/**
	 * 设置serverAddress
	 *
	 * @param serverAddress serverAddress
	 */
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	/**
	 * 获取checkTime
	 *
	 * @return checkTime
	 */
	@Basic
	@Column(name = "check_time")
	public String getCheckTime() {
		return checkTime;
	}

	/**
	 * 设置checkTime
	 *
	 * @param checkTime checkTime
	 */
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	/**
	 * 获取updateTime
	 *
	 * @return updateTime
	 */
	@Basic
	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置updateTime
	 *
	 * @param updateTime updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Proxyip proxyip = (Proxyip) o;

		if (id != proxyip.id) return false;
		if (ipAddress != null ? !ipAddress.equals(proxyip.ipAddress) : proxyip.ipAddress != null) return false;
		if (ipPort != null ? !ipPort.equals(proxyip.ipPort) : proxyip.ipPort != null) return false;
		if (serverAddress != null ? !serverAddress.equals(proxyip.serverAddress) : proxyip.serverAddress != null)
			return false;
		if (checkTime != null ? !checkTime.equals(proxyip.checkTime) : proxyip.checkTime != null) return false;
		if (updateTime != null ? !updateTime.equals(proxyip.updateTime) : proxyip.updateTime != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
		result = 31 * result + (ipPort != null ? ipPort.hashCode() : 0);
		result = 31 * result + (serverAddress != null ? serverAddress.hashCode() : 0);
		result = 31 * result + (checkTime != null ? checkTime.hashCode() : 0);
		result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
		return result;
	}

	public Proxyip() {
	}

	public Proxyip(String ipAddress, String ipPort, String serverAddress, String checkTime, Date updateTime) {
		this.ipAddress = ipAddress;
		this.ipPort = ipPort;
		this.serverAddress = serverAddress;
		this.checkTime = checkTime;
		this.updateTime = updateTime;
	}

	public Proxyip(int id, String ipAddress, String ipPort, String serverAddress, String checkTime, Date updateTime) {
		this.id = id;
		this.ipAddress = ipAddress;
		this.ipPort = ipPort;
		this.serverAddress = serverAddress;
		this.checkTime = checkTime;
		this.updateTime = updateTime;
	}

}
