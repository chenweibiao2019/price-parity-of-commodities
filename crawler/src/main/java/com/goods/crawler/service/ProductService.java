package com.goods.crawler.service;

import com.goods.crawler.entity.Product;
import com.goods.crawler.repository.ProductRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

/**
 * <pre>类名: ProductService</pre>
 * <pre>描述: 爬虫Facade类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/1/23 14:37</pre>
 * <pre>作者: chenwb</pre>
 */
@Service
public class ProductService {

	private static Logger logger = LoggerFactory.getLogger(ProductService.class);

	@Autowired
	private ProductRepository productRepository;

	/**
	 * @param product
	 * @return void
	 * @Description: 保存一个商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:39
	 */
	@Transactional
	public void saveProductInfo(Product product, Product productTemp, @Nullable String key) {
		if (productTemp != null) {
			productTemp.setProductId(product.getProductId());
			productTemp.setProductName(product.getProductName());
			productTemp.setCategoryId(product.getCategoryId());
			productTemp.setCategoryName(product.getCategoryName());
			productTemp.setSellCount(product.getSellCount());
			productTemp.setReviewCount(product.getReviewCount());
			productTemp.setCollectCount(product.getCollectCount());
			productTemp.setStock(product.getStock());
			productTemp.setDeliveryAdd(product.getDeliveryAdd());
			productTemp.setShopId(product.getShopId());
			productTemp.setShopName(product.getShopName());
			productTemp.setShopdsrMs(product.getShopdsrMs());
			productTemp.setShopdsrFw(product.getShopdsrFw());
			productTemp.setShopdsrWl(product.getShopdsrWl());
			productTemp.setProductSku(product.getProductSku());
			productTemp.setProductUrl(product.getProductUrl());
			productTemp.setProductDetail(product.getProductDetail());
			productTemp.setProductScore(product.getProductScore());
			productTemp.setRecommendedReason(product.getRecommendedReason());
			productTemp.setPrice(productTemp.getPrice() + product.getPrice());
			productTemp.setProductImg(productTemp.getProductImg() + product.getProductImg());
			productTemp.setUpdateTime(productTemp.getUpdateTime() + product.getUpdateTime());
			productTemp.setSpareField1(product.getSpareField1());
			if(!StringUtils.isEmpty(key)){
				productTemp.setCategoryName(key);
			}
			productTemp.setSpareField2("1");
			productRepository.saveAndFlush(productTemp);
			//更新索引
		} else {
			if(!StringUtils.isEmpty(key)){
				productTemp.setCategoryName(key);
			}
			product.setSpareField2("1");
			productRepository.saveAndFlush(product);
		}

	}

	/**
	 * @param product
	 * @return void
	 * @Description: 保存一个商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:39
	 */
	@Transactional
	public void saveProductInfoAdd(Product product, Product productTemp,String key) {
		if (productTemp != null) {
			productTemp.setProductId(product.getProductId());
			productTemp.setProductName(product.getProductName());
			productTemp.setCategoryId(product.getCategoryId());
			productTemp.setCategoryName(product.getCategoryName());
			productTemp.setSellCount(product.getSellCount());
			productTemp.setReviewCount(product.getReviewCount());
			productTemp.setCollectCount(product.getCollectCount());
			productTemp.setStock(product.getStock());
			productTemp.setDeliveryAdd(product.getDeliveryAdd());
			productTemp.setShopId(product.getShopId());
			productTemp.setShopName(product.getShopName());
			productTemp.setShopdsrMs(product.getShopdsrMs());
			productTemp.setShopdsrFw(product.getShopdsrFw());
			productTemp.setShopdsrWl(product.getShopdsrWl());
			productTemp.setProductSku(product.getProductSku());
			productTemp.setProductUrl(product.getProductUrl());
			productTemp.setProductDetail(product.getProductDetail());
			productTemp.setProductScore(product.getProductScore());
			productTemp.setRecommendedReason(product.getRecommendedReason());
			productTemp.setPrice(productTemp.getPrice() + product.getPrice());
			productTemp.setProductImg(productTemp.getProductImg() + product.getProductImg());
			productTemp.setUpdateTime(productTemp.getUpdateTime() + product.getUpdateTime());
			productTemp.setSpareField1(product.getSpareField1());
			productTemp.setSpareField2(key);
			try{
				productRepository.saveAndFlush(productTemp);
			}catch (Exception e){
				//
			}
			//更新索引
		} else {
			product.setSpareField2(key);
			try{
			productRepository.saveAndFlush(product);
			}catch (Exception e){
				//
			}
		}

	}

	/**
	 * @param productId
	 * @return Product
	 * @Description: 根据商品id获取商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:45
	 */
	@Transactional
	public Product findProductByProductId(String productId) {
		List<Product> products = productRepository.findProductsByProductId(productId);
		if (products != null && products.size() >= 1) {
			return products.get(0);
		}
		return null;
	}


	/**
	 * @param driver
	 * @param url
	 * @return Product
	 * @Description: 根据商品详情页的url爬取商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:26
	 */
	public Product crawlingOne(WebDriver driver, String url) {
		Product product = null;
		product = new Product();

		driver.get(url);
		new WebDriverWait(driver, 20, 1500).until(ExpectedConditions.presenceOfElementLocated(By.className("sn-cart-link")));
		try {
			String productId = driver.findElement(By.id("LineZing")).getAttribute("itemid");
			product.setProductId(productId == null ? "" : productId);
		} catch (Exception e) {
			product.setProductId("");
		}
		try {
			String productName = driver.findElement(By.className("tb-detail-hd")).findElement(By.tagName("h1")).getText();
			product.setProductName(productName == null ? "" : productName);
		} catch (Exception e) {
			product.setProductName("");
		}
		try {
			String curPrice = driver.findElements(By.className("tm-price")).get(driver.findElements(By.className("tm-price")).size() - 1).getText();
			product.setPrice(curPrice == null ? "," : curPrice + ","); //new PricePO(String.valueOf(new Date()), curPrice)
			product.setSpareField1(Double.parseDouble(curPrice));
		} catch (Exception e) {
			product.setPrice("");
		}
		// TODO:商品分类
		// String url1 = driver.findElements(By.tagName("script")).get(6).getAttribute("src");
		try {
			String sellCount = driver.findElements(By.className("tm-count")).get(0).getText();
			product.setSellCount(sellCount == null ? "" : sellCount);
		} catch (Exception e) {
			product.setSellCount("");
		}
		try {
			String reviewCount = driver.findElements(By.className("tm-count")).get(1).getText();
			product.setReviewCount(reviewCount == null ? "" : reviewCount);
		} catch (Exception e) {
			product.setReviewCount("");
		}
		try {
			String collectCount = driver.findElements(By.className("tm-count")).get(2).getText();
			product.setCollectCount(collectCount == null ? "" : collectCount);
		} catch (Exception e) {
			product.setCollectCount("");
		}
		try {
			int stock = Integer.parseInt(driver.findElement(By.id("J_EmStock")).getText().substring(2, driver.findElement(By.id("J_EmStock")).getText().length() - 1));
			product.setStock(stock);
		} catch (Exception e) {
			product.setStock(0);
		}
		try {
			String deliveryAdd = driver.findElement(By.id("J_deliveryAdd")).getText();
			product.setDeliveryAdd(deliveryAdd == null ? "" : deliveryAdd);
		} catch (Exception e) {
			product.setDeliveryAdd("");
		}
		try {
			String shopId = driver.findElement(By.id("LineZing")).getAttribute("shopid");
			product.setShopId(shopId == null ? "" : shopId);
		} catch (Exception e) {
			product.setShopId("");
		}
		try {
			//优化店铺名获取方式
			String shopName = driver.findElement(By.className("slogo-shopname")).findElement(By.tagName("strong")).getText();
//			String shopName = driver.findElement(By.id("side-shop-info")).findElement(By.tagName("a")).getText();
			product.setShopName(shopName == null ? "" : shopName);
		} catch (Exception e) {
			product.setShopName("");
		}
		try {
			String productImg = driver.findElement(By.id("J_ImgBooth")).getAttribute("src");
			product.setProductImg(productImg == null ? "," : productImg + ","); //new ImgPo("", driver.findElement(By.id("J_ImgBooth")).getAttribute("src"))
		} catch (Exception e) {
			product.setProductImg("");
		}
		try {
			double shopdsrMs = Double.parseDouble(driver.findElements(By.className("shopdsr-score-con")).get(0).getText());
			product.setShopdsrMs(shopdsrMs);
		} catch (Exception e) {
			product.setShopdsrMs(0.0);
		}
		try {
			double shopdsrFw = Double.parseDouble(driver.findElements(By.className("shopdsr-score-con")).get(1).getText());
			product.setShopdsrFw(shopdsrFw);
		} catch (Exception e) {
			product.setShopdsrFw(0.0);
		}
		try {
			double shopdsrWl = Double.parseDouble(driver.findElements(By.className("shopdsr-score-con")).get(2).getText());
			product.setShopdsrWl(shopdsrWl);
		} catch (Exception e) {
			product.setShopdsrWl(0.0);
		}

		// TODO:商品sku
		product.setProductUrl(url);
		product.setUpdateTime(String.valueOf(new Date()) + ",");
		// TODO:商品详情
		logger.info(product.toString());
		return product;
	}
	/**
	 * @param driver
	 * @return curPageNum
	 * @Description: 搜索结果页获取当前页码
	 * @author chenwb
	 * @date 2019/1/23 14:29
	 */
	public int getCurPageNum(WebDriver driver) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='text' and @name='jumpto']")));
		String curPageNum = driver.findElement(By.className("ui-page-skip")).findElement(By.xpath("//input[@type='text' and @name='jumpto']")).getAttribute("value");
		return Integer.parseInt(curPageNum);
	}

	/**
	 * @param driver
	 * @return int
	 * @Description: 搜索结果页获取总页数
	 * @author chenwb
	 * @date 2019/1/23 15:05
	 */
	public int getPageTotal(WebDriver driver) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='hidden' and @name='totalPage']")));
		String pageTotal = driver.findElement(By.className("ui-page-skip")).findElement(By.xpath("//input[@type='hidden' and @name='totalPage']")).getAttribute("value");
		return Integer.parseInt(pageTotal);
	}

	/**
	 * @param driver
	 * @return void
	 * @Description: 点击下一页
	 * @author chenwb
	 * @date 2019/1/23 15:13
	 */
	public void toNextPage(WebDriver driver) {
		new WebDriverWait(driver, 10, 3000).until(ExpectedConditions.presenceOfElementLocated(By.className("ui-page-next")));
		driver.findElement(By.className("ui-page-next")).click();
	}

	/**
	 * @Description: 获取所有商品信息
	 * @author chenwb
	 * @date 2019/2/20 15:40
	 * @return List<Product>
	 */
	@Transactional
	public List<Product> findAllProducts(){
		return productRepository.findProductByIdGreaterThanEqual(17063);
	}
}
