package com.goods.crawler.service;

import com.goods.crawler.api.DTO.CrawlerRequestDTO;
import com.goods.crawler.entity.Category;
import com.goods.crawler.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>类名: CategoryService</pre>
 * <pre>描述: 商品种类服务类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/1/27 9:45</pre>
 * <pre>作者: chenwb</pre>
 */
@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	/**
	 * @Description: 批量保存商品种类
	 * @author chenwb
	 * @date 2019/1/27 17:44
	 * @param categories
	 * @return void
	 */
	@Transactional
	public void saveCategory(List<Category> categories) {
		categoryRepository.saveAll(categories);
	}

	/**
	 * @return List<String>
	 * @Description: 查询所有商品种类，供商品爬取搜索用
	 * @author chenwb
	 * @date 2019/1/27 17:43
	 */
	@Transactional
	public List<String> findAllCategory() {
		List<Category> categories = categoryRepository.findCategoriesByIdBetween(136270,137248);
		List<String> keys = new ArrayList<String>();
		for (Category category : categories) {
			keys.add(category.getCategory3Name());
		}
		return keys;
	}
}
