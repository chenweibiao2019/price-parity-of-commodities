package com.goods.crawler.service;

import com.goods.crawler.entity.Product;
import com.goods.crawler.repository.ProductRepository;
import com.goods.crawler.utils.ValidationUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.util.Date;
import java.util.List;

/**
 * <pre>类名: ProductJDService</pre>
 * <pre>描述: 爬虫JDService类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/15 15:10</pre>
 * <pre>作者: chenwb</pre>
 */
@Service
public class ProductJDService {

	private static Logger logger = LoggerFactory.getLogger(ProductJDService.class);

	@Autowired
	private ProductRepository productRepository;

	/**
	 * @param product
	 * @return void
	 * @Description: 保存一个商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:39
	 */
	@Transactional
	public void saveProductInfo(Product product, Product productTemp,String key) {
		try{
			ValidationUtil.validate(product);
		}catch (ValidationException e){
			product.setSpareField3(e.getMessage());
			productTemp.setSpareField3(e.getMessage());
		}

		if (productTemp != null) {
			productTemp.setProductId(product.getProductId());
			productTemp.setProductName(product.getProductName());
			productTemp.setCategoryId(product.getCategoryId());
			productTemp.setCategoryName(product.getCategoryName());
			productTemp.setSellCount(product.getSellCount());
			productTemp.setReviewCount(product.getReviewCount());
			productTemp.setCollectCount(product.getCollectCount());
			productTemp.setStock(product.getStock());
			productTemp.setDeliveryAdd(product.getDeliveryAdd());
			productTemp.setShopId(product.getShopId());
			productTemp.setShopName(product.getShopName());
			productTemp.setShopdsrMs(product.getShopdsrMs());
			productTemp.setShopdsrFw(product.getShopdsrFw());
			productTemp.setShopdsrWl(product.getShopdsrWl());
			productTemp.setProductSku(product.getProductSku());
			productTemp.setProductUrl(product.getProductUrl());
			productTemp.setProductDetail(product.getProductDetail());
			productTemp.setProductScore(product.getProductScore());
			productTemp.setRecommendedReason(product.getRecommendedReason());
			productTemp.setPrice(productTemp.getPrice() + product.getPrice());
			productTemp.setProductImg(productTemp.getProductImg() + product.getProductImg());
			productTemp.setUpdateTime(productTemp.getUpdateTime() + product.getUpdateTime());
			productTemp.setSpareField1(productTemp.getSpareField1());
			productTemp.setCategoryName(key);
			productTemp.setSpareField2("2");
			productRepository.saveAndFlush(productTemp);
			//更新索引
		} else {
			product.setCategoryName(key);
			product.setSpareField2("2");
			productRepository.saveAndFlush(product);
		}

	}

	/**
	 * @param productId
	 * @return Product
	 * @Description: 根据商品id获取商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:45
	 */
	@Transactional
	public Product findProductByProductId(String productId) {
		List<Product> products = productRepository.findProductsByProductId(productId);
		if (products != null && products.size() >= 1) {
			return products.get(0);
		}
		return null;
	}

	/**
	 * @param driver
	 * @param url
	 * @return Product
	 * @Description: 根据商品详情页的url爬取商品信息
	 * @author chenwb
	 * @date 2019/1/23 14:26
	 */
	public Product crawlingOne(WebDriver driver, String url) {
		Product product = null;
		product = new Product();
		driver.get(url);
		new WebDriverWait(driver, 20, 1500).until(ExpectedConditions.presenceOfElementLocated(By.className("dt")));
		try {
			String productId = driver.findElement(By.className("left-btns")).findElement(By.tagName("a")).getAttribute("data-id");
			product.setProductId(productId == null ? "" : productId);
		} catch (Exception e) {
			product.setProductId("");
		}
		try {
			String productName = driver.findElement(By.className("sku-name")).getText();
			product.setProductName(productName == null ? "" : productName);
		} catch (Exception e) {
			product.setProductName("");
		}
		try {
			String curPrice = driver.findElements(By.className("p-price")).get(0).findElements(By.tagName("span")).get(1).getText();
			product.setPrice(curPrice == null ? "," : curPrice + ","); //new PricePO(String.valueOf(new Date()), curPrice)
			product.setSpareField1(Double.parseDouble(curPrice));
		} catch (Exception e) {
			product.setPrice("");
		}
		// TODO:商品分类
		// String url1 = driver.findElements(By.tagName("script")).get(6).getAttribute("src");
		try {
			String sellCount = "";  //jd hasn't sellCount.
			product.setSellCount(sellCount == null ? "" : sellCount);
		} catch (Exception e) {
			product.setSellCount("100");
		}
		try {
			String reviewCount = driver.findElement(By.id("comment-count")).findElement(By.tagName("a")).getText();
			product.setReviewCount(reviewCount == null ? "" : reviewCount);
		} catch (Exception e) {
			product.setReviewCount("100");
		}
		try {
			String collectCount = ""; //jd hasn't collectCount.
			product.setCollectCount(collectCount == null ? "" : collectCount);
		} catch (Exception e) {
			product.setCollectCount("100");
		}
		try {
			int stock = -1;
			product.setStock(stock);
		} catch (Exception e) {
			product.setStock(0);
		}
		try {
			String deliveryAdd = driver.findElement(By.id("summary-service")).getText().split("从")[1].split("发货")[0];
			product.setDeliveryAdd(deliveryAdd == null ? "" : deliveryAdd);
		} catch (Exception e) {
			product.setDeliveryAdd("");
		}
		try {
			String shopId = driver.findElements(By.className("btns")).get(0).findElements(By.tagName("a")).get(1).getAttribute("data-vid");
			product.setShopId(shopId == null ? "" : shopId);
		} catch (Exception e) {
			product.setShopId("");
		}
		try {
			//优化店铺名获取方式
			String shopName = driver.findElement(By.className("hl_red")).getText();
//			String shopName = driver.findElement(By.id("side-shop-info")).findElement(By.tagName("a")).getText();
			product.setShopName(shopName == null ? "" : shopName);
		} catch (Exception e) {
			product.setShopName("");
		}
		try {
			String productImg = driver.findElement(By.id("spec-img")).getAttribute("src");
			product.setProductImg(productImg == null ? "," : productImg + ","); //new ImgPo("", driver.findElement(By.id("J_ImgBooth")).getAttribute("src"))
		} catch (Exception e) {
			product.setProductImg("");
		}
/*		try{
			driver.findElement(By.className("u-jd"));
			product.setShopdsrMs(5.0);
			product.setShopdsrFw(5.0);
			product.setShopdsrWl(5.0);
		}catch (Exception e1){*/
			try {
				double shopdsrMs = Double.parseDouble(driver.findElement(By.className("score-parts")).findElements(By.className("score-detail")).get(0).findElement(By.tagName("em")).getAttribute("title").split("分")[0]);
				product.setShopdsrMs(shopdsrMs);
			} catch (Exception e) {
				product.setShopdsrMs(0.0);
			}
			try {
				double shopdsrFw = Double.parseDouble(driver.findElement(By.className("score-parts")).findElements(By.className("score-detail")).get(2).findElement(By.tagName("em")).getAttribute("title").split("分")[0]);
				product.setShopdsrFw(shopdsrFw);
			} catch (Exception e) {
				product.setShopdsrFw(0.0);
			}
			try {
				double shopdsrWl = Double.parseDouble(driver.findElement(By.className("score-parts")).findElements(By.className("score-detail")).get(1).findElement(By.tagName("em")).getAttribute("title").split("分")[0]);
				product.setShopdsrWl(shopdsrWl);
			} catch (Exception e) {
				product.setShopdsrWl(0.0);
			}
	/*	}*/

		// TODO:商品sku
		product.setProductUrl(url);
		product.setUpdateTime(String.valueOf(new Date()) + ",");
		// TODO:商品详情
		logger.info(product.toString());
		return product;
	}

	/**
	 * @param driver
	 * @return curPageNum
	 * @Description: 搜索结果页获取当前页码
	 * @author chenwb
	 * @date 2019/1/23 14:29
	 */
	public int getCurPageNum(WebDriver driver) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.className("p-wrap")));
		String curPageNum = driver.findElement(By.className("p-num")).findElement(By.className("curr")).getText();
		return Integer.parseInt(curPageNum);
	}

	/**
	 * @param driver
	 * @return int
	 * @Description: 搜索结果页获取总页数
	 * @author chenwb
	 * @date 2019/1/23 15:05
	 */
	public int getPageTotal(WebDriver driver) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.className("p-skip")));
		String pageTotal = driver.findElement(By.className("p-skip")).findElement(By.tagName("b")).getText();
		return Integer.parseInt(pageTotal);
	}

	/**
	 * @param driver
	 * @return void
	 * @Description: 点击下一页
	 * @author chenwb
	 * @date 2019/1/23 15:13
	 */
	public void toNextPage(WebDriver driver) {
		new WebDriverWait(driver, 10, 3000).until(ExpectedConditions.presenceOfElementLocated(By.className("pn-next")));
		driver.findElement(By.className("pn-next")).click();
	}
}
