package com.goods.evaluate.serviceImpl;

import com.goods.evaluate.entity.Product;
import com.goods.evaluate.service.TestService1;
import com.goods.evaluate.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class TestService implements TestService1 {

	@Autowired
	private ProductRepository productRepository;

	@Transactional
	public void testSave(){
		Optional<Product> product = productRepository.findById(10);
		productRepository.save(product.orElse(null));
		System.out.println("保存。。。");
	}

	@Transactional
	public void testUpdate(){
		productRepository.updateProduct("test10");
		System.out.println("更新。。。");
	}
}
