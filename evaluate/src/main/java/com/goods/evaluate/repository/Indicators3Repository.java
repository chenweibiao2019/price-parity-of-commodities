package com.goods.evaluate.repository;

import com.goods.evaluate.entity.Indicators3;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface Indicators3Repository extends JpaRepository<Indicators3,Integer>, CrudRepository<Indicators3,Integer> {
}
