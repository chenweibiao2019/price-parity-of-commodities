package com.goods.evaluate.facade;


import com.goods.evaluate.service.EvaluateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvaluateFacade {

	private static Logger logger = LoggerFactory.getLogger(EvaluateFacade.class);

	@Autowired
	EvaluateService evaluateService;

/*	public void additional(){
		evaluateService.additional();
	}*/
	public void additional(){
		evaluateService.additional();
	}

	public void evaluate(){
		 evaluateService.evaluate();
	}
}
