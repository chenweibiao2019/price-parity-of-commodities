package com.goods.evaluate.controller;

import com.goods.evaluate.service.TestService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

	@Autowired
	TestService1 testService;

	@RequestMapping("/testSave")
	@ResponseBody
	public void testSave(){
		testService.testSave();
	}

	@RequestMapping("/testUpdate")
	@ResponseBody
	public void testUpdate(){
		testService.testUpdate();
	}
}
