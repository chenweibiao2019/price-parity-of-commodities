package com.goods.evaluate.controller;


import com.goods.evaluate.facade.EvaluateFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
* <pre>类名: EvaluateController</pre>
* <pre>描述: 商品评价控制器</pre>
* <pre>版权: 浙江理工大学信息学院</pre>
* <pre>日期: 2019/2/20 14:10</pre>
* <pre>作者: chenwb</pre>
*/

@Controller
@RequestMapping("/evaluate")
public class EvaluateController {

	private static Logger logger = LoggerFactory.getLogger(EvaluateController.class);

	@Autowired
	EvaluateFacade evaluateFacade;

	/**
	 * @Description: 处理补充商品数据
	 * @author chenwb
	 * @date 2019/2/20 15:17
	 * @return void
	 */
	@RequestMapping("/additional")
	public void additional(){
 		evaluateFacade.additional();
	}

  /**
   * @Description: 商品评分
   * @author chenwb
   * @date 2019/2/21 14:51
   * @return void
   */
  @RequestMapping("/evaluate")
  public void evaluate(){
	  evaluateFacade.evaluate();
  }

}
