package com.goods.evaluate.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Test {
	public static void main (String[] args ) throws IOException {
		String a ="{\"name\":\"hello\",\"body\":{\"age\":\"12\",\"height\":\"33\"}}";
		ObjectMapper objectMapper = new ObjectMapper();
		Person person = objectMapper.readValue(a, Person.class);
//		System.out.println("name: "+person.name);
		System.out.println("age: "+person.body.age);
		System.out.println("height: "+person.body.height);
		String labelString = objectMapper.writeValueAsString(person);
		System.out.println(labelString);
	}

	public static class Person{
/*		@JsonProperty("name")
		public String name;*/

		@JsonProperty("body")
		public Body body;
		public Person(){
		}
	}
	public static class Body{
		@JsonProperty("age")
		public String age;

		@JsonProperty("height")
		public String height;

		public Body() {
		}
	}
}
