package com.goods.evaluate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

@SpringBootApplication
@EnableSpringConfigured
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class EvaluateApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluateApplication.class, args);
	}

}
