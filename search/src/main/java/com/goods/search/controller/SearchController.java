package com.goods.search.controller;

import com.goods.search.facade.SearchFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * <pre>类名: SearchController</pre>
 * <pre>描述: 搜索控制器</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/1/25 10:00</pre>
 * <pre>作者: chenwb</pre>
 */

@Controller
@RequestMapping("/search")

public class SearchController {

	private static Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	private SearchFacade searchFacade;


	/**
	 * @return void
	 * @Description: 建立全量索引任务
	 * @author chenwb
	 * @date 2019/1/25 10:01
	 */
   @RequestMapping("/indexAll")
	public void indexAll() {
		searchFacade.indexAll();
	}
}
/*
	@RequestMapping("/search")
	public List<Product> search(@RequestBody SearchRequestDTO searchRequestDTO){
		return searchFacade.search(searchRequestDTO);
	}
}
*/
