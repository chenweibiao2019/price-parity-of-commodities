package com.goods.search.facade;

import com.goods.search.api.DTO.SearchRequestDTO;
import com.goods.search.entity.Product;
import com.goods.search.serviceImpl.IndexServiceImpl;
import com.goods.search.serviceImpl.SearchServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* <pre>类名: SearchFacade</pre>
* <pre>描述: 搜索模块facade</pre>
* <pre>版权: 浙江理工大学信息学院</pre>
* <pre>日期: 2019/1/26 10:16</pre>
* <pre>作者: chenwb</pre>
*/
@Service
public class SearchFacade {

	private static Logger logger = LoggerFactory.getLogger(SearchFacade.class);

	@Autowired
	private IndexServiceImpl indexService;

	@Scheduled(cron = "0 0 0/5 * * ?")
	public void indexAll(){
		logger.info("开始建立全量索引");
		indexService.indexAll();
	}

}
