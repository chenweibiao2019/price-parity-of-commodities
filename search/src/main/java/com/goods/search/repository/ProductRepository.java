package com.goods.search.repository;

import com.goods.search.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <pre>类名: ProductsRepository</pre>
 * <pre>描述: 商品jpa接口类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/1/20 20:54</pre>
 * <pre>作者: chenwb</pre>
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product>, PagingAndSortingRepository<Product, Integer> {

	List<Product> findProductsByProductId(String productId);

	@Modifying
	@Query(value = "UPDATE Product p SET p.price= :price,p.productImg=:productImg,p.updateTime=:updateTime WHERE p.productId= :id")
	int updateByProductId(@Param("id") String id, @Param("price") String price, @Param("productImg") String productImg, @Param("updateTime") String updateTime);
}
