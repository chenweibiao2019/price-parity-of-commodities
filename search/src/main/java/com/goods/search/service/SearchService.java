package com.goods.search.service;

import com.goods.search.api.DTO.HistoryPriceRequestDTO;
import com.goods.search.api.DTO.HistoryPriceResponseDTO;
import com.goods.search.api.DTO.SearchRequestDTO;
import com.goods.search.api.DTO.SearchResponseDTO;

public interface SearchService {

	 SearchResponseDTO search(SearchRequestDTO searchRequestDTO);

	 HistoryPriceResponseDTO historyPrice(HistoryPriceRequestDTO historyPriceRequestDTO);
}
