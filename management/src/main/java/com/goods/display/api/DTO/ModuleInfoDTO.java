package com.goods.display.api.DTO;

import java.io.Serializable;

public class ModuleInfoDTO implements Serializable {

	private String moduleName;
	private String moduleIp;
	private String modulePort;
	private Integer moduleLatency;
	private Double occupiedRateOfBandwidth;
	private Double occupiedRateOfMemory;
	private Double occupiedRateOfCpu;
	private Boolean moduleCondition;

	/**
	 * 获取moduleName
	 *
	 * @return moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * 设置moduleName
	 *
	 * @param moduleName moduleName
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * 获取moduleIp
	 *
	 * @return moduleIp
	 */
	public String getModuleIp() {
		return moduleIp;
	}

	/**
	 * 设置moduleIp
	 *
	 * @param moduleIp moduleIp
	 */
	public void setModuleIp(String moduleIp) {
		this.moduleIp = moduleIp;
	}

	/**
	 * 获取moduleLatency
	 *
	 * @return moduleLatency
	 */
	public Integer getModuleLatency() {
		return moduleLatency;
	}

	/**
	 * 设置moduleLatency
	 *
	 * @param moduleLatency moduleLatency
	 */
	public void setModuleLatency(Integer moduleLatency) {
		this.moduleLatency = moduleLatency;
	}

	/**
	 * 获取occupiedRateOfBandwidth
	 *
	 * @return occupiedRateOfBandwidth
	 */
	public Double getOccupiedRateOfBandwidth() {
		return occupiedRateOfBandwidth;
	}

	/**
	 * 设置occupiedRateOfBandwidth
	 *
	 * @param occupiedRateOfBandwidth occupiedRateOfBandwidth
	 */
	public void setOccupiedRateOfBandwidth(Double occupiedRateOfBandwidth) {
		this.occupiedRateOfBandwidth = occupiedRateOfBandwidth;
	}

	/**
	 * 获取occupiedRateOfMemory
	 *
	 * @return occupiedRateOfMemory
	 */
	public Double getOccupiedRateOfMemory() {
		return occupiedRateOfMemory;
	}

	/**
	 * 设置occupiedRateOfMemory
	 *
	 * @param occupiedRateOfMemory occupiedRateOfMemory
	 */
	public void setOccupiedRateOfMemory(Double occupiedRateOfMemory) {
		this.occupiedRateOfMemory = occupiedRateOfMemory;
	}

	/**
	 * 获取occupiedRateOfCpu
	 *
	 * @return occupiedRateOfCpu
	 */
	public Double getOccupiedRateOfCpu() {
		return occupiedRateOfCpu;
	}

	/**
	 * 设置occupiedRateOfCpu
	 *
	 * @param occupiedRateOfCpu occupiedRateOfCpu
	 */
	public void setOccupiedRateOfCpu(Double occupiedRateOfCpu) {
		this.occupiedRateOfCpu = occupiedRateOfCpu;
	}

	/**
	 * 获取moduleCondition
	 *
	 * @return moduleCondition
	 */
	public Boolean getModuleCondition() {
		return moduleCondition;
	}

	/**
	 * 设置moduleCondition
	 *
	 * @param moduleCondition moduleCondition
	 */
	public void setModuleCondition(Boolean moduleCondition) {
		this.moduleCondition = moduleCondition;
	}

	/**
	 * 获取modulePort
	 *
	 * @return modulePort
	 */
	public String getModulePort() {
		return modulePort;
	}

	/**
	 * 设置modulePort
	 *
	 * @param modulePort modulePort
	 */
	public void setModulePort(String modulePort) {
		this.modulePort = modulePort;
	}
}
