package com.goods.management.service;

import com.goods.management.api.DTO.IndicatorDTO;
import com.goods.management.api.DTO.Indicators2DTO;
import com.goods.management.api.DTO.PageRequest;
import com.goods.management.api.DTO.ResponseDTO;
import com.goods.management.entity.Moduleinfo;
import com.goods.management.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * <pre>类名: ManagementService</pre>
 * <pre>描述: 后台管理接口类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/28 15:03</pre>
 * <pre>作者: chenwb</pre>
 */
public interface ManagementService {

	/**
	 * @return List<Moduleinfo>
	 * @Description: 查询所有模块信息
	 * @author chenwb
	 * @date 2019/2/28 15:03
	 */
	List<Moduleinfo> findAllModuleInfo();

	/**
	 * @Description: 分页查询所有模块信息
	 * @author chenwb
	 * @date 2019/3/1 11:03
	 * @return Page
	/*
	Page<Moduleinfo> findAllModuleInfoByPage(Integer pageNum, Integer pageSize);*/

	/**
	 * @param moduleinfo
	 * @return ResponseDTO
	 * @Description: 保存更新模块信息
	 * @author chenwb
	 * @date 2019/2/28 15:04
	 */
	ResponseDTO saveModuleInfo(Moduleinfo moduleinfo);

	/**
	 * @param id
	 * @return ResponseDTO
	 * @Description: 根据id删除模块信息
	 * @author chenwb
	 * @date 2019/3/4 11:16
	 */
	ResponseDTO deleteModuleByid(Integer id);

	/**
	 * @return IndicatorDTO
	 * @Description: 获取评价模型
	 * @author chenwb
	 * @date 2019/3/6 15:24
	 */
	IndicatorDTO findEvaluateModule();

	/**
	 * @param indicators2DTO
	 * @return ResponseDTO
	 * @Description: 保存评价模型
	 * @author chenwb
	 * @date 2019/3/7 10:01
	 */
	ResponseDTO saveEvaluate(Indicators2DTO indicators2DTO);

	/**
	 * @Description: 校验商品信息完整
	 * @author chenwb
	 * @date 2019/3/7 10:51
	 * @param pageNum pageSize
	 * @return 输出参数
	 */
	PageRequest<Product> validateProduct(Integer pageNum, Integer pageSize);

	/**
	 * @Description: 更新非法商品信息
	 * @author chenwb
	 * @date 2019/3/8 10:45
	 * @return ResponseDTO
	 */
	ResponseDTO updatavalidateProduct();

	/**
	 * @Description: 保存商品信息
	 * @author chenwb
	 * @date 2019/3/8 9:09
	 * @param product
	 * @return ResponseDTO
	 */
	ResponseDTO saveProduct(Product product);

	/**
	 * @Description: 删除非法商品信息
	 * @author chenwb
	 * @date 2019/3/8 14:20
	 * @param id
	 * @return ResponseDTO
	 */
	ResponseDTO deleteInvalid(Integer id);
}
