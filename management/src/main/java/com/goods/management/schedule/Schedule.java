package com.goods.management.schedule;

import com.goods.management.entity.Moduleinfo;
import com.goods.management.jms.Producer;
import com.goods.management.repository.ModuleInfoRepository;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.jms.Destination;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <pre>类名: Schedule</pre>
 * <pre>描述: 定时任务类</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/25 17:15</pre>
 * <pre>作者: chenwb</pre>
 */
@Service
public class Schedule {
	private static Logger logger = LoggerFactory.getLogger(Schedule.class);

	@Autowired
	Producer producer;

	@Autowired
	ModuleInfoRepository moduleInfoRepository;

	/**
	 * @return 输出参数
	 * @Description: 定时获取各模块的运行信息
	 * @author chenwb
	 * @date 2019/2/25 17:14
	 */
	@Scheduled(cron = "0/4 * * * * ?")
	public void getModulesInfo() {
		Destination destination = new ActiveMQTopic("getModulesInfo.topic");
		producer.sendMessage(destination, "getModulesInfo");
	}

	@Scheduled(cron = "0/8 * * * * ?")
	public void moduleCondition() {
		List<Moduleinfo> moduleinfoList = moduleInfoRepository.findAll();
		ArrayList<Moduleinfo> moduleinfos = new ArrayList<Moduleinfo>();
		for(Moduleinfo moduleinfo:moduleinfoList){
			if(System.currentTimeMillis() - moduleinfo.getUpdateTime().getTime() >= 16000){
				moduleinfo.setModuleCondition(false);
				moduleinfos.add(moduleinfo);
			}
		}
		moduleInfoRepository.saveAll(moduleinfos);
	}
}
