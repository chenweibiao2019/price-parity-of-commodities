package com.goods.management.repository;

import com.goods.management.entity.Indicators2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Indicators2Repository extends JpaRepository<Indicators2,Integer> {
}
