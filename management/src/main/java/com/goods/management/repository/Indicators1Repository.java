package com.goods.management.repository;

import com.goods.management.entity.Indicators1;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Indicators1Repository extends JpaRepository<Indicators1,Integer> {
}
