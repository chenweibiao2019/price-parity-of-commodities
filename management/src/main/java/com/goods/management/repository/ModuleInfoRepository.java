package com.goods.management.repository;

import com.goods.management.entity.Moduleinfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ModuleInfoRepository extends JpaRepository<Moduleinfo,Integer>, PagingAndSortingRepository<Moduleinfo,Integer> {

	List<Moduleinfo> findModuleinfosByModuleIpAndModuleNameAndModulePort(String moduleIp,String moduleName,String modulePort);

	List<Moduleinfo> findModuleinfosByModuleIpAndModuleName(String moduleIp,String moduleName);
}
