package com.goods.management.repository;

import com.goods.management.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface InvalidProductRepository extends JpaRepository<Product,Integer>, CrudRepository<Product,Integer> {
}
