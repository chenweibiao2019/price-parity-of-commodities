package com.goods.management.repository;

import com.goods.management.entity.Indicators3;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Indicators3Repository extends JpaRepository<Indicators3,Integer> {
}
