package com.goods.management.utils.Validation;

import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.ConstraintViolation;
import java.util.Set;

public class ValidationUtil {

	private static Validator validator;

	static {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	public static <T> void validate(T t,Class<?>... groups) throws ValidationException {
		Set<ConstraintViolation<T>> set = validator.validate(t,groups);
		StringBuilder validateError = new StringBuilder();
		if(set.size()>0){
			for(ConstraintViolation constraintViolation : set){
				validateError.append(constraintViolation.getMessage()+";");
			}
			throw new ValidationException(validateError.toString());
		}
	}
}
