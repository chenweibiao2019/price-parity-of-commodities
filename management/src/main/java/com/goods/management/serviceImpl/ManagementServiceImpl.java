package com.goods.management.serviceImpl;

import com.goods.management.api.DTO.IndicatorDTO;
import com.goods.management.api.DTO.Indicators2DTO;
import com.goods.management.api.DTO.PageRequest;
import com.goods.management.api.DTO.ResponseDTO;
import com.goods.management.entity.*;
import com.goods.management.repository.*;
import com.goods.management.service.ManagementService;
import com.goods.management.utils.ResponseUtil;
import com.goods.management.utils.Validation.ValidationUtil;
import com.goods.management.utils.Validation.groups.InvalidProductGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class ManagementServiceImpl implements ManagementService {
	private static Logger logger = LoggerFactory.getLogger(ManagementServiceImpl.class);

	@Autowired
	ModuleInfoRepository moduleInfoRepository;

	@Autowired
	Indicators1Repository indicators1Repository;

	@Autowired
	Indicators2Repository indicators2Repository;

	@Autowired
	Indicators3Repository indicators3Repository;

	@Autowired
	ProductRepository productRepository;

/*	@Override
	public Page<Moduleinfo> findAllModuleInfoByPage(Integer pageNum, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNum, pageSize);
		return moduleInfoRepository.findAll(pageable);
	}*/

	@Override
	@Transactional
	public List<Moduleinfo> findAllModuleInfo() {
		return moduleInfoRepository.findAll();
	}

	@Override
	@Transactional
	public ResponseDTO saveModuleInfo(Moduleinfo moduleinfo) {
		ResponseDTO responseDTO = new ResponseDTO();
		List<Moduleinfo> moduleinfos = moduleInfoRepository.findModuleinfosByModuleIpAndModuleNameAndModulePort(moduleinfo.getModuleIp(), moduleinfo.getModuleName(), moduleinfo.getModulePort());
		if (moduleinfos.size() != 0) {
			responseDTO.setCode("1");
			responseDTO.setReturnMessage("模块信息已存在，不需要操作");
			return responseDTO;
		}
		moduleinfo.setUpdateTime(new Date());
		moduleInfoRepository.save(moduleinfo);
		responseDTO.setCode("0");
		responseDTO.setReturnMessage("保存成功");
		return responseDTO;
	}

	@Override
	@Transactional
	public ResponseDTO deleteModuleByid(Integer id) {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			moduleInfoRepository.deleteById(id);
		} catch (Exception e) {
			responseDTO.setCode("1");
			responseDTO.setReturnMessage("删除失败");
		}
		responseDTO.setCode("0");
		responseDTO.setReturnMessage("删除成功");
		return responseDTO;

	}

	@Override
	@Transactional
	public IndicatorDTO findEvaluateModule() {

		//统一指标为同一格式
		List<Indicators1> indicators1s = indicators1Repository.findAll();
		List<IndicatorDTO> indicatorDTOS = new ArrayList<IndicatorDTO>();
		for (Indicators1 indicators1 : indicators1s) {
			indicatorDTOS.add(indicatorToDto(indicators1, 1));
		}
		List<Indicators2> indicators2s = indicators2Repository.findAll();
		for (Indicators2 indicators2 : indicators2s) {
			indicatorDTOS.add(indicatorToDto(indicators2, 2));
		}
		List<Indicators3> indicators3s = indicators3Repository.findAll();
		for (Indicators3 indicators3 : indicators3s) {
			indicatorDTOS.add(indicatorToDto(indicators3, 3));
		}
		//id为0的为一级指标
		Iterator iterator = indicatorDTOS.iterator();
		List<IndicatorDTO> indicatorDTOsStep1 = new ArrayList<IndicatorDTO>();
		List<IndicatorDTO> indicatorDTOsStep2 = new ArrayList<IndicatorDTO>();
		List<IndicatorDTO> indicatorDTOsStep3 = new ArrayList<IndicatorDTO>();
		List<IndicatorDTO> indicatorDTOSTemp = new ArrayList<IndicatorDTO>();
		for (Indicators1 indicators1 : indicators1s) {
			indicatorDTOSTemp.add(indicatorToDto(indicators1, 1));
		}
		IndicatorDTO indicatorDTO = new IndicatorDTO("评价模型", 1.0, 0, 0, indicatorDTOSTemp);
		for (IndicatorDTO indicatorDTO1 : indicatorDTO.getChildren()) {
			indicatorDTO1.setChildren(iterator(indicatorDTOS.iterator(), 2, indicatorDTO1));
		}

		for (IndicatorDTO indicatorDTO1 : indicatorDTO.getChildren()) {
			for (IndicatorDTO indicatorDTO2 : indicatorDTO1.getChildren())
				indicatorDTO2.setChildren(iterator(indicatorDTOS.iterator(), 3, indicatorDTO1));
		}
		return indicatorDTO;
	}

	@Override
	@Transactional
	public ResponseDTO saveEvaluate(Indicators2DTO indicators2DTO) {
		List<Indicators2> indicators2s = indicators2Repository.findAll();
		for (Indicators2 indicators2 : indicators2s) {
			if ("销量".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getSellCount());
			} else if ("评论量".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getReviewCount());
			} else if ("收藏量".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getCollectCount());
			} else if ("描述评分".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getShopdsrMs());
			} else if ("服务评分".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getShopdsrFw());
			} else if ("物流评分".equals(indicators2.getIndicators2Name())) {
				indicators2.setIndicators2Weight(indicators2DTO.getShopdsrWl());
			}
		}
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			indicators2Repository.saveAll(indicators2s);
		} catch (Exception e) {
			responseDTO.setCode("0");
			responseDTO.setReturnMessage("保存失败");
			return responseDTO;
		}
		responseDTO.setCode("1");
		responseDTO.setReturnMessage("保存成功");
		return responseDTO;
	}

	@Override
	@Transactional
	public PageRequest<Product> validateProduct(Integer pageNum, Integer pageSize) {
		List<Product> invalidProducts = productRepository.findAllInvalidProducts();
		PageRequest<Product> pageRequest = new PageRequest<Product>();
		if (pageNum == 0) {
			pageRequest.setFirst(true);
		}
		if (invalidProducts.size() / pageSize == pageNum) {
			pageRequest.setHasNext(false);
		}
		pageRequest.setContent(invalidProducts.subList(pageNum * pageSize, invalidProducts.size() - pageNum * pageSize > pageSize ? (pageNum + 1) * pageSize : invalidProducts.size()));
		pageRequest.setPageNum(pageNum);
		pageRequest.setPageSize(pageSize);
		return pageRequest;
	}

	@Override
	@Transactional
	public ResponseDTO updatavalidateProduct() {
		List<Product> products = productRepository.findAll();
		List<Product> invalidProducts = new ArrayList<Product>();

		for (Product product : products) {
			try {
				ValidationUtil.validate(product, InvalidProductGroup.class);
			} catch (ValidationException e) {
				product.setSpareField3(e.getMessage());
				invalidProducts.add(product);
			}
		}
		try {
			productRepository.saveAll(invalidProducts);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.fail();
		}
		return ResponseUtil.success();
	}

	@Override
	@Transactional
	public ResponseDTO saveProduct(Product product) {
		try {
			product.setSpareField3("");
			productRepository.save(product);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.fail();
		}
		return ResponseUtil.success();
	}

	@Override
	@Transactional
	public ResponseDTO deleteInvalid(Integer id) {
		try {
			productRepository.deleteById(id);
		} catch (Exception e) {
			return ResponseUtil.fail();
		}
		return ResponseUtil.success();
	}

	private List<IndicatorDTO> iterator(Iterator iterator, Integer step, IndicatorDTO indicatorDTO) {
		List<IndicatorDTO> indicatorDTOS = new ArrayList<IndicatorDTO>();
		while (iterator.hasNext()) {
			IndicatorDTO indicatorDTOTemp = (IndicatorDTO) iterator.next();
			/*System.out.println(indicatorDTOTemp.getStep() == step);
			System.out.println(indicatorDTOTemp.getId() == indicatorDTO.getId());*/
			if (step.equals(indicatorDTOTemp.getStep()) && indicatorDTOTemp.getId().equals(indicatorDTO.getId())) {
				indicatorDTOS.add(indicatorDTOTemp);
				iterator.remove();
			}
		}
		return indicatorDTOS;
	}

	private IndicatorDTO indicatorToDto(Object o, Integer step) {
		IndicatorDTO indicatorDTO = new IndicatorDTO();
		Class c = o.getClass();
		Field[] declareFields = c.getDeclaredFields();
		try {
			for (Field field : declareFields) {
				field.setAccessible(true); //
				if (field.getName().endsWith("Name")) {
					indicatorDTO.setName((String) field.get(o));
				} else if (field.getName().endsWith("Weight")) {
					indicatorDTO.setWeight((Double) field.get(o));
				} else if (field.getName().endsWith("Id")) {
					indicatorDTO.setId((Integer) field.get(o));
				}
				if (step == 1 && "id".equals(field.getName())) {
					indicatorDTO.setId((Integer) field.get(o));
				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			logger.error("indicatorToDto转换失败");
		}
		indicatorDTO.setStep(step);
		return indicatorDTO;
	}
}
