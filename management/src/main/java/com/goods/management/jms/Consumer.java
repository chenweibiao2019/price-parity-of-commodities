package com.goods.management.jms;

import com.goods.management.utils.DifferenceHelper;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.*;

/*@Service
public class Consumer implements MessageListener {

	@Autowired
	DifferenceHelper differenceHelper;

	ActiveMQConnectionFactory connectionfactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
	//创建与JMS服务的连接:ConnectionFactory被管理的对象，由客户端创建，用来创建一个连接对象
	Connection connection;//获取连接，connection一个到JMS系统提供者的活动连接

	{
		try {
			connection = connectionfactory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);//打开会话，一个单独的发送和接受消息的线程上下文
			Queue queue1 = new ActiveMQQueue("modulesInfo.queue");
			MessageConsumer consumer1 = session.createConsumer(queue1);
			consumer1.setMessageListener(new Consumer());
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		*//**
		 * 接受文本类型的消息
		 *//*
		if (message instanceof TextMessage) { //instanceof 测试它所指向的对象是否是TextMessage类
			TextMessage text = (TextMessage) message;
			try {
				System.out.println("发送的文本消息内容为：" + text.getText()); //接受文本消息
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
*//**
 * 接受Map类型的消息
 *//*
		if (message instanceof MapMessage) {
			MapMessage map = (MapMessage) message;
			try {
				System.out.println("姓名：" + map.getString("name"));
				System.out.println("是否是英雄：" + map.getBoolean("IsHero"));
				System.out.println("年龄:" + map.getInt("age"));
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
		if (message instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				ModuleInfoDTO moduleInfoDTO = (ModuleInfoDTO) objMsg.getObject();
				System.out.println(moduleInfoDTO.toString());
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
}*/
