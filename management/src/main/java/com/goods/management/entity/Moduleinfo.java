package com.goods.management.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Moduleinfo {
	private int id;
	private String moduleName;
	private String moduleIp;
	private String modulePort;
	private Integer moduleLatency;
	private String occupiedRateOfBandwidth;
	private String occupiedRateOfMemory;
	private String occupiedRateOfCpu;
	private Boolean moduleCondition;
	private Date updateTime;

	/**
	 * 获取id
	 *
	 * @return id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	/**
	 * 设置id
	 *
	 * @param id id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 获取moduleName
	 *
	 * @return moduleName
	 */
	@Basic
	@Column(name = "module_name")
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * 设置moduleName
	 *
	 * @param moduleName moduleName
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * 获取moduleIp
	 *
	 * @return moduleIp
	 */
	@Basic
	@Column(name = "module_ip")
	public String getModuleIp() {
		return moduleIp;
	}

	/**
	 * 设置moduleIp
	 *
	 * @param moduleIp moduleIp
	 */
	public void setModuleIp(String moduleIp) {
		this.moduleIp = moduleIp;
	}

	/**
	 * 获取modulePort
	 *
	 * @return modulePort
	 */
	@Basic
	@Column(name = "module_port")
	public String getModulePort() {
		return modulePort;
	}

	/**
	 * 设置modulePort
	 *
	 * @param modulePort modulePort
	 */
	public void setModulePort(String modulePort) {
		this.modulePort = modulePort;
	}

	/**
	 * 获取moduleLatency
	 *
	 * @return moduleLatency
	 */
	@Basic
	@Column(name = "module_latency")
	public Integer getModuleLatency() {
		return moduleLatency;
	}

	/**
	 * 设置moduleLatency
	 *
	 * @param moduleLatency moduleLatency
	 */
	public void setModuleLatency(Integer moduleLatency) {
		this.moduleLatency = moduleLatency;
	}

	/**
	 * 获取occupiedRateOfBandwidth
	 *
	 * @return occupiedRateOfBandwidth
	 */
	@Basic
	@Column(name = "occupiedrate_of_bandwidth")
	public String getOccupiedRateOfBandwidth() {
		return occupiedRateOfBandwidth;
	}

	/**
	 * 设置occupiedRateOfBandwidth
	 *
	 * @param occupiedRateOfBandwidth occupiedRateOfBandwidth
	 */
	public void setOccupiedRateOfBandwidth(String occupiedRateOfBandwidth) {
		this.occupiedRateOfBandwidth = occupiedRateOfBandwidth;
	}

	/**
	 * 获取occupiedRateOfMemory
	 *
	 * @return occupiedRateOfMemory
	 */
	@Basic
	@Column(name = "occupiedrate_of_memory")
	public String getOccupiedRateOfMemory() {
		return occupiedRateOfMemory;
	}

	/**
	 * 设置occupiedRateOfMemory
	 *
	 * @param occupiedRateOfMemory occupiedRateOfMemory
	 */
	public void setOccupiedRateOfMemory(String occupiedRateOfMemory) {
		this.occupiedRateOfMemory = occupiedRateOfMemory;
	}

	/**
	 * 获取occupiedRateOfCpu
	 *
	 * @return occupiedRateOfCpu
	 */
	@Basic
	@Column(name = "occupiedrate_of_cpu")
	public String getOccupiedRateOfCpu() {
		return occupiedRateOfCpu;
	}

	/**
	 * 设置occupiedRateOfCpu
	 *
	 * @param occupiedRateOfCpu occupiedRateOfCpu
	 */
	public void setOccupiedRateOfCpu(String occupiedRateOfCpu) {
		this.occupiedRateOfCpu = occupiedRateOfCpu;
	}

	/**
	 * 获取moduleCondition
	 *
	 * @return moduleCondition
	 */
	@Basic
	@Column(name = "module_condition")
	public Boolean getModuleCondition() {
		return moduleCondition;
	}

	/**
	 * 设置moduleCondition
	 *
	 * @param moduleCondition moduleCondition
	 */
	public void setModuleCondition(Boolean moduleCondition) {
		this.moduleCondition = moduleCondition;
	}

	/**
	 * 获取updateTime
	 *
	 * @return updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置updateTime
	 *
	 * @param updateTime updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Moduleinfo that = (Moduleinfo) o;

		if (id != that.id) return false;
		if (moduleName != null ? !moduleName.equals(that.moduleName) : that.moduleName != null) return false;
		if (moduleIp != null ? !moduleIp.equals(that.moduleIp) : that.moduleIp != null) return false;
		if (moduleLatency != null ? !moduleLatency.equals(that.moduleLatency) : that.moduleLatency != null) return false;
		if (occupiedRateOfBandwidth != null ? !occupiedRateOfBandwidth.equals(that.occupiedRateOfBandwidth) : that.occupiedRateOfBandwidth != null)
			return false;
		if (occupiedRateOfMemory != null ? !occupiedRateOfMemory.equals(that.occupiedRateOfMemory) : that.occupiedRateOfMemory != null)
			return false;
		if (occupiedRateOfCpu != null ? !occupiedRateOfCpu.equals(that.occupiedRateOfCpu) : that.occupiedRateOfCpu != null)
			return false;
		if (moduleCondition != null ? !moduleCondition.equals(that.moduleCondition) : that.moduleCondition != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (moduleName != null ? moduleName.hashCode() : 0);
		result = 31 * result + (moduleIp != null ? moduleIp.hashCode() : 0);
		result = 31 * result + (moduleLatency != null ? moduleLatency.hashCode() : 0);
		result = 31 * result + (occupiedRateOfBandwidth != null ? occupiedRateOfBandwidth.hashCode() : 0);
		result = 31 * result + (occupiedRateOfMemory != null ? occupiedRateOfMemory.hashCode() : 0);
		result = 31 * result + (occupiedRateOfCpu != null ? occupiedRateOfCpu.hashCode() : 0);
		result = 31 * result + (moduleCondition != null ? moduleCondition.hashCode() : 0);
		return result;
	}

	public Moduleinfo() {
	}

	public Moduleinfo(String moduleName, String moduleIp, String modulePort, Integer moduleLatency, String occupiedRateOfBandwidth, String occupiedRateOfMemory, String occupiedRateOfCpu, Boolean moduleCondition, Date updateTime) {
		this.moduleName = moduleName;
		this.moduleIp = moduleIp;
		this.modulePort = modulePort;
		this.moduleLatency = moduleLatency;
		this.occupiedRateOfBandwidth = occupiedRateOfBandwidth;
		this.occupiedRateOfMemory = occupiedRateOfMemory;
		this.occupiedRateOfCpu = occupiedRateOfCpu;
		this.moduleCondition = moduleCondition;
		this.updateTime = updateTime;
	}

}
