package com.goods.management.entity;

import javax.persistence.*;

@Entity
public class Indicators3 {
	private int id;
	private String indicators3Name;
	private Double indicators3Weight;
	private Integer indicators2Id;
	private String spareField1;

	/**
	 * 获取id
	 *
	 * @return id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	/**
	 * 设置id
	 *
	 * @param id id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 获取indicators3Name
	 *
	 * @return indicators3Name
	 */
	@Basic
	@Column(name = "Indicators_3_name")
	public String getIndicators3Name() {
		return indicators3Name;
	}

	/**
	 * 设置indicators3Name
	 *
	 * @param indicators3Name indicators3Name
	 */
	public void setIndicators3Name(String indicators3Name) {
		this.indicators3Name = indicators3Name;
	}

	/**
	 * 获取indicators3Weight
	 *
	 * @return indicators3Weight
	 */
	@Basic
	@Column(name = "Indicators_3_weight")
	public Double getIndicators3Weight() {
		return indicators3Weight;
	}

	/**
	 * 设置indicators3Weight
	 *
	 * @param indicators3Weight indicators3Weight
	 */
	public void setIndicators3Weight(Double indicators3Weight) {
		this.indicators3Weight = indicators3Weight;
	}

	/**
	 * 获取indicators2Id
	 *
	 * @return indicators2Id
	 */
	@Basic
	@Column(name = "Indicators_2_id")
	public Integer getIndicators2Id() {
		return indicators2Id;
	}

	/**
	 * 设置indicators2Id
	 *
	 * @param indicators2Id indicators2Id
	 */
	public void setIndicators2Id(Integer indicators2Id) {
		this.indicators2Id = indicators2Id;
	}

	/**
	 * 获取spareField1
	 *
	 * @return spareField1
	 */
	@Basic
	@Column(name = "spare_field1")
	public String getSpareField1() {
		return spareField1;
	}

	/**
	 * 设置spareField1
	 *
	 * @param spareField1 spareField1
	 */
	public void setSpareField1(String spareField1) {
		this.spareField1 = spareField1;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Indicators3 that = (Indicators3) o;

		if (id != that.id) return false;
		if (indicators3Name != null ? !indicators3Name.equals(that.indicators3Name) : that.indicators3Name != null)
			return false;
		if (indicators3Weight != null ? !indicators3Weight.equals(that.indicators3Weight) : that.indicators3Weight != null)
			return false;
		if (indicators2Id != null ? !indicators2Id.equals(that.indicators2Id) : that.indicators2Id != null) return false;
		if (spareField1 != null ? !spareField1.equals(that.spareField1) : that.spareField1 != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (indicators3Name != null ? indicators3Name.hashCode() : 0);
		result = 31 * result + (indicators3Weight != null ? indicators3Weight.hashCode() : 0);
		result = 31 * result + (indicators2Id != null ? indicators2Id.hashCode() : 0);
		result = 31 * result + (spareField1 != null ? spareField1.hashCode() : 0);
		return result;
	}
}
