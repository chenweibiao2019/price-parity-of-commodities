package com.goods.management.controller;


import com.goods.management.api.DTO.*;
import com.goods.management.entity.Moduleinfo;
import com.goods.management.entity.Product;
import com.goods.management.service.ManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <pre>类名: ManagementController</pre>
 * <pre>描述: 后台管理系统控制器</pre>
 * <pre>版权: 浙江理工大学信息学院</pre>
 * <pre>日期: 2019/2/25 10:34</pre>
 * <pre>作者: chenwb</pre>
 */
@Controller
@RequestMapping("/management")
public class ManagementController {

	private static Logger logger = LoggerFactory.getLogger(ManagementController.class);

	@Autowired
	ManagementService managementService;

	/**
	 * @Description: 页面跳转
	 * @author chenwb
	 * @date 2019/3/1 10:58
	 * @param pageName
	 * @return String
	 */
	@RequestMapping("/{pageName}")
	public String toIndex(@PathVariable("pageName") String pageName) {
		return pageName;
	}

	/**
	 * @Description: 保存模块信息
	 * @author chenwb
	 * @date 2019/3/1 10:59
	 * @param moduleinfo
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/saveModuleInfo", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO saveModuleInfo(@RequestBody Moduleinfo moduleinfo){
		return managementService.saveModuleInfo(moduleinfo);
	}

	/**
	 * @Description: 查询所有模块信息
	 * @author chenwb
	 * @date 2019/3/1 11:01
	 * @return 输出参数
	 */
	@RequestMapping(value = "/findAllModuleInfo", method = RequestMethod.POST)
	@ResponseBody
	public List<Moduleinfo> findAllModuleInfo(){
		return managementService.findAllModuleInfo();
	}

	/**
	 * @Description: 根据id删除模块信息
	 * @author chenwb
	 * @date 2019/3/4 11:16
	 * @param id
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/deleteModuleById", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO deleteModuleByid(Integer id){
		return managementService.deleteModuleByid(id);
	}

	/**
	 * @Description: 获取评价模型
	 * @author chenwb
	 * @date 2019/3/6 15:24
	 * @return IndicatorDTO
	 */
	@RequestMapping(value = "/findEvaluateModule", method = RequestMethod.POST)
	@ResponseBody
	public IndicatorDTO findEvaluateModule(){
		return managementService.findEvaluateModule();
	}

	/**
	 * @Description: 评价模型修改
	 * @author chenwb
	 * @date 2019/3/7 9:50
	 * @param indicators2DTO
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/saveEvaluate", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO saveEvaluate(@RequestBody Indicators2DTO indicators2DTO){
		System.out.println(indicators2DTO);
		return managementService.saveEvaluate(indicators2DTO);
	}

	/**
	 * @Description: 校验商品信息完整
	 * @author chenwb
	 * @date 2019/3/7 10:51
	 * @param pageNum pageSize
	 * @return 输出参数
	 */
	@RequestMapping(value = "/validateProduct", method = RequestMethod.POST)
	@ResponseBody
	PageRequest<Product> validateProduct(Integer pageNum, Integer pageSize){
		return managementService.validateProduct(pageNum,pageSize);
	}

	/**
	 * @Description: 更新非法商品信息
	 * @author chenwb
	 * @date 2019/3/8 10:45
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/updatavalidateProduct", method = RequestMethod.POST)
	@ResponseBody
	ResponseDTO updatavalidateProduct(){
		return managementService.updatavalidateProduct();
	}

	/**
	 * @Description: 保存商品信息
	 * @author chenwb
	 * @date 2019/3/8 9:09
	 * @param product
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO saveProduct(@RequestBody Product product){
		return managementService.saveProduct(product);
	}

	/**
	 * @Description: 删除非法商品信息
	 * @author chenwb
	 * @date 2019/3/8 14:20
	 * @param id
	 * @return ResponseDTO
	 */
	@RequestMapping(value = "/deleteInvalid", method = RequestMethod.POST)
	@ResponseBody
	ResponseDTO deleteInvalid(Integer id){
		return managementService.deleteInvalid(id);

	}

/*	@RequestMapping(value = "/findAllModuleInfoByPage", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO findAllModuleInfoByPage(@RequestParam("pageNum") Integer PageNum,@RequestParam("pageSize") Integer PageSize){
		return null;
	}*/

}
