var vm = new Vue({
  el: "#myModal",
  data: {
    sellCount:"",
    reviewCount:"",
    collectCount:"",
    shopdsrMs:"",
    shopdsrFw:"",
    shopdsrWl:""

  },
  created: function () {
    getEvaluateModule();
  },
  methods:{
    saveEvaluate:function () {
      $.ajax({
        url: "http://localhost:8801/management/saveEvaluate",
        type: "post",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        data:JSON.stringify({
            sellCount:this.sellCount,
            reviewCount:this.reviewCount,
            collectCount:this.collectCount,
            shopdsrMs:this.shopdsrMs,
            shopdsrFw:this.shopdsrFw,
            shopdsrWl:this.shopdsrWl
        })
      }).success(function (res) {
        alert(res.returnMessage);
        window.location.reload();
      })
    }
  }

})

function getEvaluateModule() {
  $.ajax({
    url: "http://localhost:8801/management/findEvaluateModule",
    type: "post",
    contentType: "application/json;charset=UTF-8",
    dataType: "json",
  }).success(function (res) {
    console.log(res)
    var myChart = echarts.init(document.getElementById("charts"));
    res.name = res.name + ' ' + res.weight;
    echarts.util.each(res.children, function (datum, index) {
      datum.name = datum.name + ' ' + datum.weight;
      echarts.util.each(datum.children, function (child, index1) {
        child.name = child.name + ' ' + child.weight;
        if(index==0){
          switch(index1){
            case 0: vm.sellCount = child.weight;break;
            case 1: vm.reviewCount = child.weight;break;
            case 2: vm.collectCount = child.weight;break;
          }
        }
        if(index==1){
          switch(index1){
            case 0: vm.shopdsrMs = child.weight;break;
            case 1: vm.shopdsrFw = child.weight;break;
            case 2: vm.shopdsrWl = child.weight;break;
          }
        }

        echarts.util.each(child.children, function (child1, index) {
          child1.name = child1.name + ' ' + child1.weight;
        });
      });

    });

    myChart.setOption(option = {
      tooltip: {
        trigger: 'item',
        triggerOn: 'mousemove'
      },
      series: [
        {
          type: 'tree',

          data: [res],

          top: '1%',
          left: '20%',
          bottom: '1%',
          right: '20%',

          symbolSize: 7,

          label: {
            normal: {
              position: 'left',
              verticalAlign: 'middle',
              align: 'right',
              fontSize: 14
            }
          },

          leaves: {
            label: {
              normal: {
                position: 'right',
                verticalAlign: 'middle',
                align: 'left'
              }
            }
          },

          expandAndCollapse: true,
          animationDuration: 550,
          animationDurationUpdate: 750
        }
      ]
    });
  })


}

