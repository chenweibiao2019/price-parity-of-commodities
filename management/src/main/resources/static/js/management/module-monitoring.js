var PAGE_SIZE = 10

var vm = new Vue({
  el: "#myModal",
  data: {
    moduleName: "",
    moduleIp: "",
    modulePort: ""
  },
  methods: {
    addModule: function () {
      console.log("add");
      $.ajax({
        url: "http://localhost:8801/management/saveModuleInfo",
        type: "post",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        data: JSON.stringify({
          moduleName: this.moduleName,
          moduleIp: this.moduleIp,
          modulePort: this.modulePort,
          moduleLatency: "",
          occupiedRateOfBandwidth: "",
          occupiedRateOfMemory: "",
          occupiedRateOfCpu: "",
          moduleCondition: ""
        })
      }).success(function (res) {
        alert(res.returnMessage)
      })
    }
  }
})

var vm1 = new Vue({
  el: "#content",
  data() {
    return{
      moduleInfos: ""
    }
  },
  created: function () {
    getModuleInfos()
  },
  mounted: function () {
    setInterval(getModuleInfos, 8000);
  },
  methods: {
    deleteModule: function (e) {
      var target = e.target;
      var id = target.getAttribute("data-id");
      if (confirm("确认删除该模块吗?") == true) {
        deleteModuleInfoById(id)
      } else {
        return;
      }
    },
    updateModule: function (e) {
      var target = e.target;
      vm.moduleName = target.getAttribute("data-moduleName");
      vm.moduleIp = target.getAttribute("data-moduleIp");
      vm.modulePort = target.getAttribute("data-modulePort");
      $("#myModal").modal();
    }
  }
})

var vm2 = new Vue({
  el: "#tipModal",
  data: {
    tipsResult: "",
    showModel: ""
  }
})

function getModuleInfos() {
  $.ajax({
    url: "http://localhost:8801/management/findAllModuleInfo",
    type: "post",
    contentType: "application/json;charset=UTF-8",
    dataType: "json"
  }).success(function (res) {
    vm1.moduleInfos = res;
  })
}

function deleteModuleInfoById(id) {
  $.ajax({
    url: "http://localhost:8801/management/deleteModuleById",
    type: "post",
    dataType: "json",
    data: {
      id: id
    }
  }).success(function (res) {
    alert(res.returnMessage)
    window.location.reload();
  })
}

function getModuleInfosByPage(pageNum, pageSize) {
  $.ajax({
    url: "http://localhost:8801/management/saveModuleInfo",
    type: "post",
    contentType: "application/json;charset=UTF-8",
    dataType: "json",
    data: {
      pageNum: pageNum,
      pageSize: pageSize
    }
  }).success(function (res) {

  })
}
